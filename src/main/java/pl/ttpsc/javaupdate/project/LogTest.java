package pl.ttpsc.javaupdate.project;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LogTest {


    private static Logger applicationLogger = LogManager.getLogger("Application");
    private static Logger securityLogger = LogManager.getLogger("Security");

    public static void main(String[] args) {
        applicationLogger.trace("Application started.");
        securityLogger.error("Wrong credentials.");
        applicationLogger.trace("Application stopped.");

    }
}
