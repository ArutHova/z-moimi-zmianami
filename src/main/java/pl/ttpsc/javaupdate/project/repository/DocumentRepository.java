package pl.ttpsc.javaupdate.project.repository;

import pl.ttpsc.javaupdate.project.model.Document;
import pl.ttpsc.javaupdate.project.persistence.PersistenceManager;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static pl.ttpsc.javaupdate.project.db.DbConnection.getConnection;

public class DocumentRepository {

    private PersistenceManager persistence;
    private DataSource ds;

    public DocumentRepository(DataSource ds) {
        this.ds = ds;
    }



    private static final String INSERT_DOCUMENT_SQL = "INSERT INTO document " + " (name, description,creator,topic,content) VALUES "
            + " (?, ?, ?, ?, ?);";
    private static final String SELECT_DOCUMENT_BY_ID = "select id,name,description,creator,topic,content from document where id =?";
    private static final String SELECT_ALL_DOCUMENT = "select * from document";
    private static final String DELETE_DOCUMENT_SQL = "delete from document where id = ?";
    private static final String UPDATE_DOCUMENT_SQL = "update document set name = ?, description = ?, creator = ?, topic = ?, content = ? where id = ?;";


    public void insertDocument(Document document) {
        System.out.println(INSERT_DOCUMENT_SQL);
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_DOCUMENT_SQL)) {
            preparedStatement.setString(1, document.getName());
            preparedStatement.setString(2, document.getDescription());
            preparedStatement.setString(3, document.getCreator());
            preparedStatement.setString(4, document.getTopic());
            preparedStatement.setString(5, document.getContent());
            System.out.println(preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // select document by id
    public Document selectDocument(int id) {
        Document document = null;
        // step 1: Establishing a connection
        try (Connection connection = getConnection();
             // Step 2 Create a statement using connection object
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_DOCUMENT_BY_ID)) {
            preparedStatement.setInt(1, id);
            System.out.println(preparedStatement);
            // Step 3 Execute the query or update query
            ResultSet rs = preparedStatement.executeQuery();
            // Step 4 Process the ResultSet object
            while (rs.next()) {
                String name = rs.getString("name");
                String description = rs.getString("description");
                String creator = rs.getString("creator");
                String topic = rs.getString("topic");
                String content = rs.getString("content");
                document = new Document(id, name, description, creator, topic, content);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return document;
    }

    // select list documents
    public List<Document> selectAllDocuments() {

        // using try-with-resources to avoid closing resources
        List<Document> documents = new ArrayList<>();
        //Step 1 Establishing a Connection
        try (Connection connection = getConnection();

             //Step 2 Create a statement using connection object
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_DOCUMENT)) {
            System.out.println(preparedStatement);
            //Step 3 Execute the query or update query
            ResultSet rs = preparedStatement.executeQuery();

            //Step 4: Process the ResultSet object.
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String description = rs.getString("description");
                String creator = rs.getString("creator");
                String topic = rs.getString("topic");
                String content = rs.getString("content");
                documents.add(new Document(id, name, description, creator, topic, content));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return documents;
    }

    // delete document
    public boolean deleteDocument(int id) throws SQLException {
        boolean rowDeleted;
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_DOCUMENT_SQL)) {
            statement.setInt(1, id);
            rowDeleted = statement.executeUpdate() > 0;
        }
        return rowDeleted;
    }

    // update document
    public boolean updateDocument(Document document) throws SQLException {
        boolean rowUpdated;
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_DOCUMENT_SQL)) {
            statement.setString(1, document.getName());
            statement.setString(2, document.getDescription());
            statement.setString(3, document.getCreator());
            statement.setString(4, document.getTopic());
            statement.setString(5, document.getContent());

            rowUpdated = statement.executeUpdate() > 0;
        }
        return rowUpdated;
    }
}
