package pl.ttpsc.javaupdate.project.view.web;

import pl.ttpsc.javaupdate.project.model.Project;
import pl.ttpsc.javaupdate.project.service.ProjectService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name="ProjectControllerServlet", value = "/ProjectControllerServlet")
public class ProjectServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private ProjectService projectService;


    public ProjectServlet(ProjectService projectService) {
        this.projectService = projectService;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            String theCommand = request.getParameter("command");

            if (theCommand == null) {
                theCommand = "LIST";
            }
            switch (theCommand) {

                case "LIST":
                    listProjects(request, response);
                    break;
                case "ADD":
                    addProject(request, response);
                    break;
                case "LOAD":
                    loadProject(request, response);
                    break;
                case "UPDATE":
                    updateProject(request, response);
                    break;
                case "Delete":
                    deleteProject(request, response);
                    break;

                default:
                    listProjects(request, response);
            }
        } catch (Exception e) {
            throw new ServletException(e);
        }

    }

    private void updateProject(HttpServletRequest request, HttpServletResponse response) throws Exception {

        //read project info from form data
        int id = Integer.parseInt(request.getParameter("projectId"));
        String name = request.getParameter("name");
        String description = request.getParameter("description");

        //create a new student object
        Project project = new Project(id,name,description);

        // perform update on database
        projectService.updateProject(project);
    }

    private void loadProject(HttpServletRequest request, HttpServletResponse response) throws Exception {

        //read project id from form data
        int projectId = Integer.parseInt(request.getParameter("projectId"));

        // get project from db
        Project project = projectService.selectProject(projectId);

        // place student in the request attribute
        request.setAttribute("THE_PROJECT", project);

        // send to jsp page
        RequestDispatcher dispatcher = request.getRequestDispatcher("/update-project-form.jsp");
        dispatcher.forward(request, response);
    }

    private void deleteProject(HttpServletRequest request, HttpServletResponse response) throws Exception {

        //read project id from form data
        int projectId = Integer.parseInt(request.getParameter("projectId"));
        //delete project from database
        projectService.deleteProject(projectId);
        //send them back to "list projects" page
        listProjects(request, response);

    }

    private void addProject(HttpServletRequest request, HttpServletResponse response) throws Exception {

        //read project info from form data
        String name = request.getParameter("name");
        String description = request.getParameter("description");

        // create a new project object
        Project project = new Project(name, description);

        //add the project to the database
        projectService.insertProject(project);

        // send back to main page (the project list)
        listProjects(request, response);
    }

    private void listProjects(HttpServletRequest request, HttpServletResponse response) throws Exception {

        //get projects from db
        List<Project> projects = projectService.selectAllProjects();

        //add projects to the request
        request.setAttribute("PROJECT_LIST", projects);

        // send to JSP page
        RequestDispatcher dispatcher = request.getRequestDispatcher("/list-projects.jsp");
        dispatcher.forward(request, response);
    }
}
