package pl.ttpsc.javaupdate.project.model;

public class Document {

    private int id;
    private String name;
    private String description;
    private String creator;
    private String topic;
    private String content;

    public Document(int id, String name, String description, String creator, String topic, String content) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.creator = creator;
        this.topic = topic;
        this.content = content;
    }

    public Document(String name, String description, String creator, String topic, String content) {
        this.name = name;
        this.description = description;
        this.creator = creator;
        this.topic = topic;
        this.content = content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
