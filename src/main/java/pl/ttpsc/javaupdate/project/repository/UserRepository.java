package pl.ttpsc.javaupdate.project.repository;

import pl.ttpsc.javaupdate.project.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static pl.ttpsc.javaupdate.project.db.DbConnection.getConnection;

public class UserRepository {

    private static final String INSERT_USER_SQL = "INSERT INTO user " + " (name, surname) VALUES "
            + " (?, ?);";
    private static final String SELECT_ALL_USERS = "select * from user";

    public void insertUser(User user) throws SQLException {
        System.out.println(INSERT_USER_SQL);
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USER_SQL)) {
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getSurname());
            System.out.println(preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public List<User> selectAllProjects(){

        // using try-with-resources to avoid closing resources
        List<User> users = new ArrayList<>();
        //Step 1 Establishing a Connection
        try(Connection connection = getConnection();
            //Step 2 Create a statement using connection object
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_USERS)){
            System.out.println(preparedStatement);
            //Step 3 Execute the query or update query
            ResultSet rs = preparedStatement.executeQuery();
            //Step 4: Process the ResultSet object.
            while(rs.next()) {
                int id= rs.getInt("id");
                String name = rs.getString("name");
                String surname = rs.getString("surname");
                users.add(new User(id,name,surname));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return  users;
    }
}
