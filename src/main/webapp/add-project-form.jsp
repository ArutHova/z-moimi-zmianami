<!DOCTYPE html>
<html>

<head>
	<title>Add PROJECT</title>

	<link type="text/css" rel="stylesheet" href="css/style.css">
	<link type="text/css" rel="stylesheet" href="css/add-project-style.css">
</head>

<body>
	<div id="wrapper">
		<div id="header">
			<h2>PROJECT MANAGEMENT </h2>
		</div>
	</div>

	<div id="container">
		<h3>Add Project</h3>

		<form action="ProjectControllerServlet" method="GET">

			<input type="hidden" name="command" value="ADD" />

			<table>
				<tbody>
					<tr>
						<td><label>Name:</label></td>
						<td><input type="text" name="name" /></td>
					</tr>

					<tr>
						<td><label>Description:</label></td>
						<td><input type="text" name="description" /></td>
					</tr>

					<tr>
						<td><label></label></td>
						<td><input type="submit" value="Save" class="save" /></td>
					</tr>

				</tbody>
			</table>
		</form>

		<div style="clear: both;"></div>

		<p>
			<a href="ProjectControllerServlet">Back to List</a>
		</p>
	</div>
</body>

</html>