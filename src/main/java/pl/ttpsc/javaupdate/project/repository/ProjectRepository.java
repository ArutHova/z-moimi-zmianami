package pl.ttpsc.javaupdate.project.repository;

import pl.ttpsc.javaupdate.project.model.Project;
import pl.ttpsc.javaupdate.project.persistence.PersistenceManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static pl.ttpsc.javaupdate.project.db.DbConnection.getConnection;

public class ProjectRepository {

    private PersistenceManager persistence;
//    private DataSource ds;
//
//    public ProjectRepository(DataSource ds) {
//        this.ds = ds;
//    }


    private static final String INSERT_PROJECT_SQL = "INSERT INTO project " + " (name, description) VALUES "
            + " (?, ?);";
    private static final String SELECT_PROJECT_BY_ID = "select id,name,description from project where id =?";
    private static final String SELECT_ALL_PROJECT = "select * from project";
    private static final String DELETE_PROJECT_SQL = "delete from project where id = ?";
    private static final String UPDATE_PROJECT_SQL = "update project set name = ?, description = ? where id = ?;";



    public void insertProject(Project project) throws SQLException {
        System.out.println(INSERT_PROJECT_SQL);
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_PROJECT_SQL)) {
            preparedStatement.setString(1, project.getName());
            preparedStatement.setString(2, project.getDescription());
            System.out.println(preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    // select project by id
    public Project selectProject(int id) {
        Project project = null;
        // step 1: Establishing a connection
        try (Connection connection = getConnection();
             // Step 2 Create a statement using connection object
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_PROJECT_BY_ID)) {
            preparedStatement.setInt(1, id);
            // Step 3 Execute the query or update query
            ResultSet rs = preparedStatement.executeQuery();
            // Step 4 Process the ResultSet object
            while (rs.next()) {
                String name = rs.getString("name");
                String description = rs.getString("description");
                project = new Project(id, name, description);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return project;
    }

    // select all users
    public List<Project> selectAllProjects(){

        // using try-with-resources to avoid closing resources
        List<Project> projects = new ArrayList<>();
        //Step 1 Establishing a Connection
        try(Connection connection = getConnection();
            //Step 2 Create a statement using connection object
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_PROJECT)){
            System.out.println(preparedStatement);
            //Step 3 Execute the query or update query
            ResultSet rs = preparedStatement.executeQuery();
            //Step 4: Process the ResultSet object.
            while(rs.next()) {
                int id= rs.getInt("id");
                String name = rs.getString("name");
                String description = rs.getString("description");
                projects.add(new Project(id,name,description));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return  projects;
    }

    // delete project
    public boolean deleteProject(int id) throws  SQLException {
        boolean rowDeleted;
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_PROJECT_SQL)) {
            statement.setInt(1,id);
            rowDeleted = statement.executeUpdate() > 0;
        }
        return rowDeleted;
    }

    //update project
    public boolean updateProject(Project project) throws SQLException {
        boolean rowUpdated;
        try(Connection connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(UPDATE_PROJECT_SQL);) {
            statement.setString(1,project.getName());
            statement.setString(2,project.getDescription());
            statement.setInt(3,project.getId());

            rowUpdated = statement.executeUpdate() > 0;
        }
        return rowUpdated;
    }
}
