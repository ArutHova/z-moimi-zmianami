package pl.ttpsc.javaupdate.project.service;

import pl.ttpsc.javaupdate.project.model.Project;
import pl.ttpsc.javaupdate.project.repository.ProjectRepository;

import java.sql.SQLException;
import java.util.List;

public class ProjectService {

    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public void updateProject(Project project) {
        try {
            projectRepository.updateProject(project);
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    public List<Project> selectAllProjects() {
        return projectRepository.selectAllProjects();
    }

    public void insertProject(Project project) {
        try {
            projectRepository.insertProject(project);
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void deleteProject(int projectId) {
        try {
            projectRepository.deleteProject(projectId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Project selectProject(int projectId)  {
        return projectRepository.selectProject(projectId);
    }
}
