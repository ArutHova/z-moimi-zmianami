package pl.ttpsc.javaupdate.project.service;


import pl.ttpsc.javaupdate.project.model.Document;
import pl.ttpsc.javaupdate.project.repository.DocumentRepository;

import java.sql.SQLException;
import java.util.List;

public class DocumentService {

    DocumentRepository documentRepository;

    public DocumentService(DocumentRepository documentRepository) {
        this.documentRepository = documentRepository;
    }


    public void updateDocument(Document document) {
        try {
            documentRepository.updateDocument(document);
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    public List<Document> selectAllDocuments() {
        return documentRepository.selectAllDocuments();
    }

    public void insertDocument(Document document) {
            documentRepository.insertDocument(document);
    }


    public void deleteDocument(int documentId) {
        try {
            documentRepository.deleteDocument(documentId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Document selectDocument(int documentId) {
        return documentRepository.selectDocument(documentId);
    }
}
