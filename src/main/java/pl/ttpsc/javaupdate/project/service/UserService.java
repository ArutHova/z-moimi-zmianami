package pl.ttpsc.javaupdate.project.service;
import pl.ttpsc.javaupdate.project.model.User;
import pl.ttpsc.javaupdate.project.repository.UserRepository;

import java.sql.SQLException;
import java.util.List;

public class UserService {

private UserRepository userRepository;


    public List<User> selectAllUsers() {
        return userRepository.selectAllProjects();
    }

    public void insertUser(User user) {
        try {
            userRepository.insertUser(user);
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }
}
