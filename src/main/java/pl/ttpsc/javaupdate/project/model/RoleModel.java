package pl.ttpsc.javaupdate.project.model;

public class RoleModel {
    private int id;
    private String roleName;
    private User userId;
    private Project projectId;

    public RoleModel(int id, String roleName, User userId, Project projectId) {
        this.id = id;
        this.roleName = roleName;
        this.userId = userId;
        this.projectId = projectId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public Project getProjectId() {
        return projectId;
    }

    public void setProjectId(Project projectId) {
        this.projectId = projectId;
    }
}