<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>

<head>
	<title>Project Management</title>

	<link type="text/css" rel="stylesheet" href="css/style.css">
</head>

<body>

	<div id="wrapper">
		<div id="header">
			<h2>Project Management</h2>
		</div>
	</div>

	<div id="container">

		<div id="content">

			<!-- put new button: Add Project -->

			<input type="button" value="Add Project"
				   onclick="window.location.href='add-project-form.jsp'; return false;"
				   class="add-project-button"
			/>

			<table>

				<tr>
					<th>Name</th>
					<th>Description</th>
					<th>Action</th>
				</tr>

				<c:forEach var="tempProject" items="${PROJECT_LIST}">

					<!-- set up a link for each project -->
					<c:url var="tempLink" value="ProjectControllerServlet">
						<c:param name="command" value="LOAD" />
						<c:param name="projectId" value="${tempProject.id}" />
					</c:url>

					<!--  set up a link to delete a project -->
					<c:url var="deleteLink" value="ProjectControllerServlet">
						<c:param name="command" value="DELETE" />
						<c:param name="projectId" value="${tempProject.id}" />
					</c:url>

					<tr>
						<td> ${tempProject.name} </td>
						<td> ${tempProject.description} </td>
						<td>
							<a href="${tempLink}">Update</a>
							 |
							<a href="${deleteLink}"
							onclick="if (!(confirm('Are you sure you want to delete this project?'))) return false">
							Delete</a>
						</td>
					</tr>

				</c:forEach>

			</table>

		</div>

	</div>
</body>


</html>